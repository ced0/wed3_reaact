// @flow

import React from "react";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";

import "./css/Home.css";

export type Props = {
  isAuthenticated: boolean
};

const Home = ({ isAuthenticated }: Props) => (
  <div>
    <h3 className="home-title">Bank of Rapperswil</h3>
    {isAuthenticated ? (
      <Card className="home-card">
        <Card.Body className="home-text">
          <Card.Title>Welcome back!</Card.Title>
          <Link to={"/dashboard"} className="home-link">
            Dashboard
          </Link>
        </Card.Body>
      </Card>
    ) : (
      <Card className="home-card">
        <Card.Body className="home-cardBody">
          <Link to={"/login"} className="home-link">
            Login
          </Link>
          <Card.Text className="home-text">
            Register now if you do not have an account yet
          </Card.Text>
          <Link to={"/signup"} className="home-link">
            Register
          </Link>
        </Card.Body>
      </Card>
    )}
  </div>
);

export default Home;
