// @flow

import React from "react";
import { Link } from "react-router-dom";
import { Card, Table } from "react-bootstrap";

import "./css/LastTransactions.css";

import type { Transaction } from "../api";
import { getTransactions } from "../api";

type Props = {
  token: string,
  nofPayments: number
};

type State = {
  transactions: Array<Transaction>
};

class LastTransactions extends React.Component<Props, State> {
  state = {
    transactions: []
  };

  render() {
    return (
      <Card className="lastTr-card">
        <Card.Header>
          <h3>Last Transactions</h3>
        </Card.Header>
        <Card.Body className="lastTr-cardBody">
          <Table className="lastTr-table" striped>
            <thead>
              <tr>
                <th>Source</th>
                <th>Target</th>
                <th>Amount [CHF]</th>
                <th>Balance [CHF]</th>
              </tr>
            </thead>
            <tbody>
              {this.state.transactions.map((tr: Transaction) => {
                if (tr.amount && tr.total)
                  return (
                    <tr>
                      <td className="lastTr-firstColumn">{tr.from}</td>
                      <td>{tr.target}</td>
                      <td>{tr.amount.toFixed(2)}</td>
                      <td>{tr.total.toFixed(2)}</td>
                    </tr>
                  );
              })}
            </tbody>
          </Table>
          <Link to={"/transactions"} className="lastTr-link">
            All Transactions
          </Link>
        </Card.Body>
      </Card>
    );
  }

  updateTransactions() {
    getTransactions(this.props.token)
      .then((transactions: any) => {
        this.setState({ transactions: transactions.result });
      })
      .catch((error: any) => {
        console.error(error);
      });
  }

  componentDidMount() {
    this.updateTransactions();
  }

  componentWillReceiveProps(nextProps: any) {
    this.updateTransactions();
  }
}

export default LastTransactions;
