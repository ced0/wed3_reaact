// @flow

import React from "react";
import { Redirect } from "react-router-dom";

import { signup } from "../api";
import { Form, Card, Button, Alert } from "react-bootstrap";

import "./css/Signup.css";

type Props = {};

type State = {
  login: string,
  firstname: string,
  lastname: string,
  password: string,
  error: ?Error,
  redirectToReferrer: boolean,
  validated: boolean,
  invalidData: boolean
};

class Signup extends React.Component<Props, State> {
  state = {
    login: "",
    firstname: "",
    lastname: "",
    password: "",
    error: null,
    redirectToReferrer: false,
    validated: false,
    invalidData: false
  };

  handleFirstNameChanged = (event: Event) => {
    if (event.target instanceof HTMLInputElement) {
      this.setState({ firstname: event.target.value });
    }
  };

  handleLastNameChanged = (event: Event) => {
    if (event.target instanceof HTMLInputElement) {
      this.setState({ lastname: event.target.value });
    }
  };

  handleLoginChanged = (event: Event) => {
    if (event.target instanceof HTMLInputElement) {
      this.setState({ login: event.target.value });
    }
  };

  handlePasswordChanged = (event: Event) => {
    if (event.target instanceof HTMLInputElement) {
      this.setState({ password: event.target.value });
    }
  };

  handleSubmit = (event: Event) => {
    this.setState({ validated: true });
    event.preventDefault();
    const { login, firstname, lastname, password } = this.state;

    if (
      firstname.length > 0 &&
      lastname.length > 0 &&
      login.length > 2 &&
      password.length > 2
    ) {
      signup(login, firstname, lastname, password)
        .then(result => {
          console.log("Signup result ", result);
          this.setState({
            redirectToReferrer: true,
            error: null,
            invalidData: false
          });
        })
        .catch(error => this.setState({ error }));
    } else {
      this.setState({ invalidData: true });
    }
  };

  render() {
    const { redirectToReferrer, error, validated, invalidData } = this.state;

    if (redirectToReferrer) {
      return <Redirect to="/login" />;
    }

    return (
      <div>
        {error && <Alert variant={"danger"}>Oops! Something went wrong!</Alert>}
        {invalidData && (
          <Alert variant={"warning"}>
            Your inputs does not met the minimum criteria.
          </Alert>
        )}
        <Card className="sign-card">
          <Card.Header>
            <h3>Welcome to the Finance Portal</h3>
          </Card.Header>
          <Form
            noValidate
            validated={validated}
            onSubmit={e => this.handleSubmit(e)}
          >
            <Card.Body className="sign-cardBody">
              <Form.Group>
                <Form.Label className="sign-label">First name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="First name"
                  required
                  value={this.state.firstname}
                  onChange={this.handleFirstNameChanged}
                />
                <Form.Text className="text-muted">
                  {this.state.firstname.length > 0
                    ? ""
                    : "Please specify your first name"}
                </Form.Text>
              </Form.Group>

              <Form.Group>
                <Form.Label className="sign-label">Last name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Last name"
                  required
                  value={this.state.lastname}
                  onChange={this.handleLastNameChanged}
                />
                <Form.Text className="text-muted">
                  {this.state.lastname.length > 0
                    ? ""
                    : "Please specify your last name"}
                </Form.Text>
              </Form.Group>

              <Form.Group>
                <Form.Label className="sign-label">User name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="User"
                  required
                  minLength="3"
                  value={this.state.login}
                  onChange={this.handleLoginChanged}
                />
                <Form.Text className="text-muted">
                  {this.state.login.length > 2
                    ? ""
                    : "Please specify your login, at least three characters"}
                </Form.Text>
              </Form.Group>

              <Form.Group>
                <Form.Label className="sign-label">Password:</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  required
                  minLength="3"
                  value={this.state.password}
                  onChange={this.handlePasswordChanged}
                />
                <Form.Text className="text-muted">
                  {this.state.password.length > 2
                    ? ""
                    : "Please specify your password, at least three characters"}
                </Form.Text>
              </Form.Group>
              <Button onClick={this.handleSubmit}>Register</Button>
            </Card.Body>
          </Form>
        </Card>
      </div>
    );
  }
}

export default Signup;
