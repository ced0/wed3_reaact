// @flow

import React from "react";
import { Card, Form, Button } from "react-bootstrap";

import "./css/Payment.css";

import { getAccountDetails, getAccount, transfer } from "../api";

import type { User } from "../api";

type AccountDetail = {
  accountNr: string,
  amount: number,
  owner: User
};

type TargetAccount = {
  accountNr: string,
  owner: {
    firstname: string,
    lastname: string
  }
};

type Props = {
  token: string,
  user: User,
  updateTransaction: any
};

type State = {
  account: ?AccountDetail,
  target: ?TargetAccount,
  amount: ?number,
  paid: boolean,
  targetRemark: string,
  targetAccountNr: ?string,
  valid: ?boolean
};

class Payment extends React.Component<Props, State> {
  state = {
    account: null,
    target: null,
    amount: 0,
    paid: false,
    targetAccountNr: "",
    valid: null,
    targetRemark: "Please specify the target account number."
  };

  render() {
    if (this.state.paid) {
      return (
        <Card className="payment-card">
          <Card.Header>
            <h3>New Payment</h3>
          </Card.Header>
          <Form>
            <Card.Body className="payment-cardBody">
              <p className="payment-text">
                Transaction to{" "}
                {this.state.target ? this.state.target.accountNr : ""}{" "}
                succeeded!
              </p>
              <p className="payment-text">
                New balance{" "}
                {this.state.account ? this.state.account.amount : ""} CHF
              </p>
              <Button
                type="submit"
                onClick={this.reset}
                className="payment-button"
              >
                Start Over
              </Button>
            </Card.Body>
          </Form>
        </Card>
      );
    } else {
      return (
        <Card className="payment-card">
          <Card.Header>
            <h3>New Payment</h3>
          </Card.Header>
          <Card.Body className="payment-cardBody">
            <Form>
              <Form.Group>
                <Form.Label className="payment-label">From:</Form.Label>
                <Form.Control
                  type="text"
                  value={
                    this.state.account
                      ? Payment.getAccountDetail(this.state.account)
                      : ""
                  }
                  disabled
                />
              </Form.Group>

              <Form.Group>
                <Form.Label className="payment-label">To:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Target Account Number"
                  required
                  value={this.state.targetAccountNr}
                  onChange={this.onTargetAccountChange}
                />
                <Form.Text className="text-muted">
                  {this.state.targetRemark}
                </Form.Text>
              </Form.Group>

              <Form.Group>
                <Form.Label className="payment-lable">Amount [CHF]:</Form.Label>
                <Form.Control
                  type="number"
                  min="0"
                  placeholder="Amount in CHF"
                  required
                  value={this.state.amount}
                  onChange={this.onAmountChange}
                />
                <Form.Text className="text-muted">
                  {Payment.isAmountValid(this.state.amount)
                    ? ""
                    : "Please specify the account."}
                </Form.Text>
              </Form.Group>

              <Button
                type="submit"
                onClick={this.pay}
                className="payment-button"
                disabled={!this.state.valid}
              >
                Pay
              </Button>
            </Form>
          </Card.Body>
        </Card>
      );
    }
  }

  static getAccountDetail(account: AccountDetail) {
    return `${account.accountNr} (CHF ${account.amount})`;
  }

  getUserAccount() {
    getAccountDetails(this.props.token)
      .then((fromAccount: AccountDetail) => {
        this.setState({ account: fromAccount });
      })
      .catch((error: any) => {
        console.error(error);
      });
  }

  componentDidMount() {
    this.getUserAccount();
  }

  isTargetAccountValid() {
    return !!this.state.target;
  }

  onTargetAccountChange = (event: Event) => {
    if (event.target instanceof HTMLInputElement) {
      const value = event.target.value;

      this.setState({
        targetAccountNr: value
      });

      if (
        value.length > 2 &&
        (this.state.account != null && value !== this.state.account.accountNr)
      ) {
        getAccount(value, this.props.token)
          .then((account: TargetAccount) => {
            this.setState({
              target: account,
              targetRemark:
                account.owner.firstname + " " + account.owner.lastname,
              valid: Payment.isAmountValid(this.state.amount)
            });
          })
          .catch(() => {
            this.setState({
              target: null,
              targetRemark: "Unknown account number specified",
              valid: false
            });
          });
      } else {
        this.setState({
          targetRemark: "Please specify the target account number.",
          valid: false
        });
      }
    }
  };

  static isAmountValid(value: any) {
    return value !== null && !isNaN(value) && value >= 0.05;
  }

  onAmountChange = (event: Event) => {
    if (event.target instanceof HTMLInputElement) {
      const value: number = parseInt(event.target.value);

      this.setState({ amount: value });

      if (Payment.isAmountValid(value)) {
        this.setState({ valid: this.isTargetAccountValid() });
      } else {
        this.setState({ valid: false });
      }
    }
  };

  pay = (event: Event) => {
    event.preventDefault();
    if (this.state.targetAccountNr && this.state.amount) {
      transfer(this.state.targetAccountNr, this.state.amount, this.props.token)
        .then(() => {
          this.getUserAccount();
          this.setState({ paid: true });
          this.props.updateTransaction();
        })
        .catch((error: any) => {
          console.error(error);
        });
    }
  };

  reset = (event: Event) => {
    event.preventDefault();
    this.setState({
      target: null,
      targetAccountNr: null,
      amount: 0,
      paid: false,
      targetRemark: "Please specify the target account number."
    });
  };
}

export default Payment;
