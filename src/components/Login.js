// @flow

import React from "react";
import { Redirect } from "react-router-dom";
import { Card, Form, Button, Alert } from "react-bootstrap";

import "./css/Login.css";

export type Props = {
  /* Callback to submit an authentication request to the server */
  authenticate: (
    login: string,
    password: string,
    callback: (error: ?Error) => void
  ) => void,
  /* We need to know what page the user tried to access so we can
       redirect after logging in */
  location: {
    state?: {
      from: string
    }
  }
};

type State = {
  login: string,
  password: string,
  error: any,
  validated: boolean,
  redirectToReferrer: boolean,
  invalidData: boolean
};

class Login extends React.Component<Props, State> {
  state = {
    login: "",
    password: "",
    error: undefined,
    validated: false,
    redirectToReferrer: false,
    invalidData: false
  };

  handleLoginChanged = (event: Event) => {
    if (event.target instanceof HTMLInputElement) {
      this.setState({ login: event.target.value });
    }
  };

  handlePasswordChanged = (event: Event) => {
    if (event.target instanceof HTMLInputElement) {
      this.setState({ password: event.target.value });
    }
  };

  handleSubmit = (event: Event) => {
    this.setState({ validated: true });
    event.preventDefault();
    const { login, password } = this.state;
    if (login.length > 0 && password.length > 2) {
      this.props.authenticate(login, password, error => {
        if (error) {
          this.setState({ error });
        } else {
          this.setState({
            redirectToReferrer: true,
            error: null,
            invalidData: false
          });
        }
      });
    } else {
      this.setState({ invalidData: true });
    }
  };

  render() {
    const { from } = this.props.location.state || {
      from: { pathname: "/dashboard" }
    };
    const { redirectToReferrer, error, validated, invalidData } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }

    return (
      <div>
        {error && <Alert variant={"danger"}>Oops! Something went wrong!</Alert>}
        {invalidData && (
          <Alert variant={"warning"}>
            Your inputs does not met the minimum criteria.
          </Alert>
        )}
        <Card className="login-card">
          <Card.Header>
            <h3>Welcome to the Finance Portal</h3>
          </Card.Header>
          <Card.Body className="login-cardBody">
            <Form
              noValidate
              validated={validated}
              onSubmit={e => this.handleSubmit(e)}
              style={{ marginBottom: "10px" }}
            >
              <Form.Group>
                <Form.Label>User name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="User"
                  onChange={this.handleLoginChanged}
                  value={this.state.login}
                  required
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Password:</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onChange={this.handlePasswordChanged}
                  value={this.state.password}
                  required
                  minLength="3"
                />
                <Form.Text className="text-muted">
                  {this.state.password.length > 2
                    ? ""
                    : "Please specify your login, at least three characters."}
                </Form.Text>
              </Form.Group>

              <Button
                type="submit"
                className="login-button"
                onClick={this.handleSubmit}
              >
                Login
              </Button>
            </Form>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default Login;
