// @flow

import React from 'react'

import * as api from "../api";
import {Transaction} from "../api";

const ANY_MONTH = 0;
const ANY_YEAR = 'Any';

const months = [];
months[0] = "Any";
months[1] = "January";
months[2] = "February";
months[3] = "March";
months[4] = "April";
months[5] = "May";
months[6] = "June";
months[7] = "July";
months[8] = "August";
months[9] = "September";
months[10] = "October";
months[11] = "November";
months[12] = "December";

const options = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric'
};

const tableStyle = {
    marginTop: 20,
    width: '100%'
};

const headerRowStyle = {
    borderBottom: '2px solid black'
};

const headerStyle = {
    textAlign: 'left',
};


const rowStyle = {
    borderBottom: '1px solid black'
};

const cellStyle = {
    padding: "15px 15px 15px 0px"
};


class AllTransactions extends React.Component<Props, State> {

    constructor(props) {
        super(props);
        this.state = {
            transactions: [],
            selectedYear: months[0],
            selectedMonth: 0,
            loading: true,
        };
    }

    handleYearChange(event) {
        event.persist();
        let selectedYear = event.target.value;

        if (selectedYear === ANY_YEAR) {
            this.loadTransactions(selectedYear, ANY_MONTH);
            this.setState({selectedMont: ANY_MONTH, selectedYear, loading: true});
        } else {
            this.loadTransactions(selectedYear, this.state.selectedMonth);
            this.setState({selectedYear, loading: true});
        }
    }

    handleMonthChange(event) {
        event.persist();
        this.loadTransactions(this.state.selectedYear, event.target.value);
        this.setState({selectedMonth: event.target.value, loading: true});
    }

    loadTransactions(selectedYear, selectedMonth) {
        let from, to;
        if (selectedYear === ANY_YEAR) {
            api.getTransactions(this.props.token)
                .then(this.updateTransactions.bind(this));
        } else {
            if (selectedMonth === ANY_MONTH) {
                from = new Date(selectedYear, 0);
                to = new Date(selectedYear, 11, 31);
            } else {
                from = new Date(selectedYear, selectedMonth - 1);
                let lastDayOfMonth = new Date(selectedYear, selectedMonth, 0).getDate();
                to = new Date(selectedYear, selectedMonth - 1, lastDayOfMonth);
            }

            from = from.toUTCString();
            to = to.toUTCString();

            console.log('loading from,to', from, to);
            api.getTransactions(this.props.token, from, to)
                .then(this.updateTransactions.bind(this));
        }
    }

    updateTransactions(resp) {
        console.log("loaded", resp.query);
        this.setState({loading: false, transactions: resp.result})
    }

    componentDidMount(): void {
        this.loadTransactions.apply(this, ['Any', 0]);
    }

    render() {
        return (
            <div>
                <div>
                    <select value={this.state.selectedYear} onChange={this.handleYearChange.bind(this)}>
                        <option value="Any">Any</option>
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                        <option value="2017">2017</option>
                    </select>
                </div>
                <div>
                    {this.state.selectedYear !== 'Any' &&
                    <select value={this.state.selectedMonth} onChange={this.handleMonthChange.bind(this)}>
                        {months.map((month, index) => (<option key={month} value={index}>{month}</option>))}
                    </select>
                    }

                </div>
                {this.state.loading ?
                    'loading transactions' :
                    this.state.transactions.length === 0 ?
                        'no transactions' :
                        <table style={tableStyle}>
                            <thead>
                            <tr style={headerRowStyle}>
                                <th style={headerStyle}>Date</th>
                                <th style={headerStyle}>From</th>
                                <th style={headerStyle}>To</th>
                                <th style={headerStyle}>Amount</th>
                                <th style={headerStyle}>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.transactions.map(transaction => (
                                <TransactionRow key={transaction.date.toString()} transaction={transaction}/>))}
                            </tbody>
                        </table>
                }
            </div>
        );
    }
}


function TransactionRow(props: { transaction: Transaction }) {
    const transaction = props.transaction;
    const date: Date = new Date(transaction.date);
    return <tr style={rowStyle}>
        <td style={cellStyle}>{date.toLocaleDateString('de-DE', options)}</td>
        <td style={cellStyle}>{transaction.from}</td>
        <td style={cellStyle}>{transaction.target}</td>
        <td style={cellStyle}>{transaction.amount}</td>
        <td style={cellStyle}>{transaction.total}</td>
    </tr>
}

export default AllTransactions
