// @flow

import React from "react";

import Payment from "./Payment";
import LastTransactions from "./LastTransactions";

import type { User } from "../api";

export type Props = {
  token: string,
  user: User
};

type State = {
  nofPayments: number
};

class Dashboard extends React.Component<Props, State> {
  state = {
    nofPayments: 0
  };

  render() {
    return (
      <div style={{ display: "flex" }}>
        <Payment
          token={this.props.token}
          user={this.props.user}
          updateTransaction={this.incrementNofPayments.bind(this)}
        />
        <LastTransactions
          token={this.props.token}
          nofPayments={this.state.nofPayments}
        />
      </div>
    );
  }

  incrementNofPayments() {
    this.setState({
      nofPayments: this.state.nofPayments + 1
    });
  }
}

export default Dashboard;
